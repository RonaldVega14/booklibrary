package com.ronaldvega.booklibrary.recyclerview;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import com.ronaldvega.booklibrary.R;

public class CustomItemAnimator extends DefaultItemAnimator {

    @Override
    public boolean animateRemove(RecyclerView.ViewHolder holder) {

        // now let's customize the remove item animation
        // it's the same process that we did for add a book
        holder.itemView.setAnimation(AnimationUtils.loadAnimation(
                holder.itemView.getContext(),
                R.anim.viewholder_remove_anim
        ));

        return super.animateRemove(holder);
    }

    @Override
    public boolean animateAdd(RecyclerView.ViewHolder holder) {
        // this method will be called when a new item will be added to the list
        // we will handle the add animation to the item here
        // first let's create a custom animation ...

        holder.itemView.setAnimation(AnimationUtils.loadAnimation(
                holder.itemView.getContext(),
                R.anim.viewholder_add_anim
        ));
        return super.animateAdd(holder);
    }

    //we can also customized the duration of the add animation


    @Override
    public long getAddDuration() {
        //Time is in milliseconds, default time is 120ms
        return 500;
    }

    @Override
    public long getRemoveDuration() {
        return 500;
    }
}
