package com.ronaldvega.booklibrary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ronaldvega.booklibrary.model.Book;
import com.ronaldvega.booklibrary.recyclerview.BookAdapter;
import com.ronaldvega.booklibrary.recyclerview.BookCallback;
import com.ronaldvega.booklibrary.recyclerview.CustomItemAnimator;

import java.util.ArrayList;
import java.util.List;

import androidx.core.util.Pair;

public class MainActivity extends AppCompatActivity implements BookCallback {

    private RecyclerView rvBooks;
    private BookAdapter bookAdapter;
    private List<Book> mdata;
    private Button btnAddBook, btnRemoveBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        initViews();
        initmdataBooks();
        setupBookAdapter();
    }

    private void setupBookAdapter(){
        bookAdapter = new BookAdapter(mdata, this);
        rvBooks.setAdapter(bookAdapter);
    }

    private void initmdataBooks(){

        // for testing purposes I'm creating a random set of books
        // you may get your data from web service or firebase db.

        mdata = new ArrayList<>();
        mdata.add(new Book(R.drawable.el_principito));
        mdata.add(new Book(R.drawable.harry_potter));
        mdata.add(new Book(R.drawable.algoritmo_de_la_felicidad));
        mdata.add(new Book(R.drawable.ejecutivo_trotamundos));
        mdata.add(new Book(R.drawable.no_te_calles));
        mdata.add(new Book(R.drawable.book_1984));


    }

    private void initViews(){

        btnAddBook = findViewById(R.id.btn_add);
        btnRemoveBook = findViewById(R.id.btn_remove);
        rvBooks = findViewById(R.id.rv_book);
        rvBooks.setLayoutManager(new LinearLayoutManager(this));
        rvBooks.setHasFixedSize(true);

        //Setting up the custom item animator
        rvBooks.setItemAnimator(new CustomItemAnimator());

        // to test the animation we need to simulate the add book operation

        btnAddBook.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                addBook();
            }
        });

        btnRemoveBook.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                removeBook();
            }
        });

    }

    private void addBook(){
        Book book = new Book(R.drawable.algoritmo_de_la_felicidad);
        mdata.add(1, book);
        bookAdapter.notifyItemInserted(1);
    }

    private void removeBook(){
        mdata.remove(1);
        bookAdapter.notifyItemRemoved(1);
    }

    @Override
    public void onBookItemClick(int pos, ImageView imgContainer, ImageView imgBook, TextView title, TextView authorName, TextView nbpages, TextView score, RatingBar ratingBar) {
        // create intent and send book object to details activity
        Intent intent = new Intent(this, BookDetailsActivity.class);
        intent.putExtra("bookObject", mdata.get(pos));

        //shared Animation setup
        Pair<View, String> p1 = Pair.create((View)imgContainer, "containerTN"); // Second arg is the transition string name
        Pair<View, String> p2 = Pair.create((View)imgBook, "bookTN"); // Second arg is the transition string name
        Pair<View, String> p3 = Pair.create((View)title, "bookTitleTN"); // Second arg is the transition string name
        Pair<View, String> p4 = Pair.create((View)authorName, "authorTN"); // Second arg is the transition string name
        Pair<View, String> p5 = Pair.create((View)nbpages, "bookPagesTN");
        Pair<View, String> p6 = Pair.create((View)score, "scoreTN");
        Pair<View, String> p7 = Pair.create((View)ratingBar, "rateTN");

        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1, p2, p3, p4, p5, p6, p7);

        //Start activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent, optionsCompat.toBundle());
        } else{
            startActivity(intent);
        }

    }
}
