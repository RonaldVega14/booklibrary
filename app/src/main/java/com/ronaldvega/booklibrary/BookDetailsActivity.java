package com.ronaldvega.booklibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.ronaldvega.booklibrary.model.Book;

public class BookDetailsActivity extends AppCompatActivity {

    ImageView imgBook;
    RatingBar ratingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        //initView
        imgBook = findViewById(R.id.item_book_img);
        ratingBar = findViewById(R.id.item_book_ratingbar);

        //we need to got book items object

        Book item =  (Book) getIntent().getExtras().getSerializable("bookObject");

        loadBookdata(item);
    }

    private void loadBookdata(Book item){
        //bind book here
        Glide.with(this).load(item.getDrawableResource()).into(imgBook);
    }
}